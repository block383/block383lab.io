#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Block383.com'
SITENAME = u'Block383'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('need to put in telegram', '#'),
          ('need to put in  twitter etc', '#'),)

DEFAULT_PAGINATION = True
PAGINATED_DIRECT_TEMPLATES = ('blog-index',)
DIRECT_TEMPLATES = ('categories', 'index', 'blog-index', 'blog')

POST_LIMIT = 3

# Formatting for dates

DEFAULT_DATE_FORMAT = ('%d/%b/%Y %a')


# Formatting for urls

ARTICLE_URL = "{date:%Y}/{date:%m}/{slug}/"
ARTICLE_SAVE_AS = "{date:%Y}/{date:%m}/{slug}/index.html"

# Plugins


# THEME = "theme/BT3-Flat"
#THEME = "C:\\Users\\walter\\git\\block383.gitlab.io\\BT3-Flat_wMods"
THEME = "themes/BT3-Flat_wMods"

FAVICON = 'https://blcok383.com/logo.png'
ICON = 'https://gitlab.com/block383/block383.gitlab.io/raw/master/block383img/antenna-bee-bloom-395241.jpg'
SHORTCUT_ICON = 'https://gitlab.com/block383/block383.gitlab.io/raw/master/block383img/antenna-bee-bloom-395241.jpg'
HEADER_IMAGE = 'https://gitlab.com/block383/block383.gitlab.io/raw/master/block383img/antenna-bee-bloom-395241.jpg'
BACKGROUND_IMAGE = 'https://gitlab.com/block383/block383.gitlab.io/raw/master/block383img/the-honeycomb-1386687.jpg'


# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
STATIC_PATHS = [
    '.well-known/acme-challenge',
]
